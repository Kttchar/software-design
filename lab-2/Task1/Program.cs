﻿public abstract class Subscription
{
    public decimal MonthlyFee { get; set; }
    public int MinimumSubscriptionPeriod { get; set; }
    public List<string> Channels { get; set; }

    public Subscription()
    {
        Channels = new List<string>();
    }
}

public class DomesticSubscription : Subscription
{
    public DomesticSubscription()
    {
        MonthlyFee = 10;
        MinimumSubscriptionPeriod = 1;
        Channels.Add("Channel 1");
        Channels.Add("Channel 2");
    }
}

public class EducationalSubscription : Subscription
{
    public EducationalSubscription()
    {
        MonthlyFee = 5;
        MinimumSubscriptionPeriod = 6;
        Channels.Add("Educational Channel 1");
        Channels.Add("Educational Channel 2");
    }
}

public class PremiumSubscription : Subscription
{
    public PremiumSubscription()
    {
        MonthlyFee = 20;
        MinimumSubscriptionPeriod = 1;
        Channels.Add("Premium Channel 1");
        Channels.Add("Premium Channel 2");
        Channels.Add("Premium Channel 3");
    }
}

public class SubscriptionFactory
{
    public static Subscription CreateSubscription(Type subscriptionType)
    {
        if (subscriptionType == typeof(DomesticSubscription))
            return new DomesticSubscription();
        else if (subscriptionType == typeof(EducationalSubscription))
            return new EducationalSubscription();
        else if (subscriptionType == typeof(PremiumSubscription))
            return new PremiumSubscription();
        else
            throw new ArgumentException("Invalid subscription type");
    }
}

public interface ISubscriptionCreator
{
    Subscription CreateSubscription(Type subscriptionType);
}

public class WebSite : ISubscriptionCreator
{
    public Subscription CreateSubscription(Type subscriptionType)
    {
        return SubscriptionFactory.CreateSubscription(subscriptionType);
    }
}

public class MobileApp : ISubscriptionCreator
{
    public Subscription CreateSubscription(Type subscriptionType)
    {
        return SubscriptionFactory.CreateSubscription(subscriptionType);
    }
}

public class ManagerCall : ISubscriptionCreator
{
    public Subscription CreateSubscription(Type subscriptionType)
    {
        return SubscriptionFactory.CreateSubscription(subscriptionType);
    }
}

public class Program
{
    public static void Main()
    {
        ISubscriptionCreator creator = new WebSite();
        Subscription subscription = creator.CreateSubscription(typeof(DomesticSubscription));
        Console.WriteLine($"Created {subscription.GetType().Name} with monthly fee {subscription.MonthlyFee} and channels {string.Join(", ", subscription.Channels)}");

        creator = new MobileApp();
        subscription = creator.CreateSubscription(typeof(EducationalSubscription));
        Console.WriteLine($"Created {subscription.GetType().Name} with monthly fee {subscription.MonthlyFee} and channels {string.Join(", ", subscription.Channels)}");

        creator = new ManagerCall();
        subscription = creator.CreateSubscription(typeof(PremiumSubscription));
        Console.WriteLine($"Created {subscription.GetType().Name} with monthly fee {subscription.MonthlyFee} and channels {string.Join(", ", subscription.Channels)}");
    }
}
