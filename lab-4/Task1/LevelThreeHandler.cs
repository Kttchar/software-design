class LevelThreeHandler : AbstractHandler
{
    public override void Handle(int request)
    {
        if (request == 3)
        {
            Console.WriteLine("Level 3 handler is handling the request");
        }
        else
        {
            base.Handle(request);
        }
    }
}
