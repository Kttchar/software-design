
abstract class AbstractHandler : IHandler
{
    private IHandler _nextHandler;

    public IHandler SetNext(IHandler handler)
    {
        this._nextHandler = handler;
        return handler;
    }

    public virtual void Handle(int request)
    {
        if (this._nextHandler != null)
        {
            this._nextHandler.Handle(request);
        }
    }
}