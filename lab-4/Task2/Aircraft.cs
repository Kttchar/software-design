class Aircraft
{
    public string Name;
    private ICommandCentre _commandCentre;

    public Aircraft(string name, ICommandCentre commandCentre)
    {
        this.Name = name;
        this._commandCentre = commandCentre;
    }

    public void Land()
    {
        _commandCentre.RequestLanding(this);
    }

    public void TakeOff()
    {
        _commandCentre.NotifyTakeOff(this);
    }
}
