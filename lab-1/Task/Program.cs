﻿
public interface IMoney
{
    int WholePart { get; set; }
    int FractionalPart { get; set; }
    void Display();
    void SetValue(int wholePart, int fractionalPart);
}



public class Money : IMoney
{
        private int _wholePart;
        private int _fractionalPart;

        public int WholePart
        {
            get { return _wholePart; }
            set
            {
                if (value < 0)
                    throw new ArgumentException("Whole part cannot be negative");
                _wholePart = value;
            }
        }

        public int FractionalPart
        {
            get { return _fractionalPart; }
            set
            {
                if (value < 0)
                    throw new ArgumentException("Fractional part cannot be negative");
                _fractionalPart = value;
            }
        }

        public Money(int wholePart, int fractionalPart)
        {
            WholePart = wholePart;
            FractionalPart = fractionalPart;
        }

        public void Display()
        {
            Console.WriteLine($"Money: {WholePart}.{FractionalPart}");
        }

        public void SetValue(int wholePart, int fractionalPart)
        {
            WholePart = wholePart;
            FractionalPart = fractionalPart;
        }
    }



public class Product
{
    public IMoney Price { get; set; }
    public string Name { get; set; }
    public string Unit { get; set; }
    public int Quantity { get; set; }
    public DateTime ArrivalDate { get; set; }

    public Product(string name, IMoney price, string unit, int quantity, DateTime arrivalDate)
    {
        Name = name;
        Price = price;
        Unit = unit;
        Quantity = quantity;
        ArrivalDate = arrivalDate;
    }

    public void ReducePrice(int amount)
    {
        Price.WholePart -= amount;
    }
}

public class Warehouse
{
    public string Name { get; set; }
    public List<Product> Products { get; set; }

    public Warehouse(string name)
    {
        Name = name;
        Products = new List<Product>();
    }

    public void AddProduct(Product product)
    {
        Products.Add(product);
    }
}

public class Reporting
{
    public Warehouse Warehouse { get; set; }

    public Reporting(Warehouse warehouse)
    {
        Warehouse = warehouse;
    }

    public void InventoryReport()
    {
        foreach (var product in Warehouse.Products)
        {
            Console.WriteLine($"Product: {product.Name}, Price: {product.Price.WholePart}.{product.Price.FractionalPart}, Unit: {product.Unit}, Quantity: {product.Quantity}, Arrival Date: {product.ArrivalDate}");
        }
    }

    public void RegisterProductArrival(Product product)
    {
        Warehouse.AddProduct(product);
        Console.WriteLine($"Product {product.Name} has arrived at the warehouse on {product.ArrivalDate}.");
    }

    public void RegisterProductDeparture(Product product)
    {
        Warehouse.Products.Remove(product);
        Console.WriteLine($"Product {product.Name} has been removed from the warehouse.");
    }
}


class Program
{
    static void Main(string[] args)
    {
        
        IMoney money1 = new Money(100, 50);
        IMoney money2 = new Money(200, 25);
        IMoney money3 = new Money(150, 75);

        
        Product product1 = new Product("Apple", money1, "kg", 10, DateTime.Now);
        Product product2 = new Product("Orange", money2, "kg", 20, DateTime.Now);
        Product product3 = new Product("Banana", money3, "kg", 15, DateTime.Now);

        
        Warehouse warehouse = new Warehouse("Main Warehouse");

        
        Reporting reporting = new Reporting(warehouse);

        
        reporting.RegisterProductArrival(product1);
        reporting.RegisterProductArrival(product2);
        reporting.RegisterProductArrival(product3);

        
        Console.WriteLine("\nProducts in the warehouse:");
        reporting.InventoryReport();

        
        foreach (var product in warehouse.Products)
        {
            product.ReducePrice(10);
        }

        
        Console.WriteLine("\nProducts in the warehouse after price reduction:");
        reporting.InventoryReport();

        
        reporting.RegisterProductDeparture(product1);

        
        Console.WriteLine("\nProducts in the warehouse after product departure:");
        reporting.InventoryReport();

        Console.ReadLine();
    }
}


