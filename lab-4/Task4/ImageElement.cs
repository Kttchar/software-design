public class ImageElement
{
    private IImageLoadingStrategy _loadingStrategy;

    public ImageElement(IImageLoadingStrategy loadingStrategy)
    {
        _loadingStrategy = loadingStrategy;
    }

    public void SetLoadingStrategy(IImageLoadingStrategy loadingStrategy)
    {
        _loadingStrategy = loadingStrategy;
    }

    public void LoadImage(string href)
    {
        _loadingStrategy.LoadImage(href);
    }
}