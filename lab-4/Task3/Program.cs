﻿class Program
{
    static void Main(string[] args)
    {
        LightElementNode div = new LightElementNode("div", true, false);
        div.AddClass("container");
        div.AddChild(new LightTextNode("Hello, world!"));
        div.AddChild(new LightElementNode("br", true, true));
        div.AddChild(new LightTextNode("It`s LightHTML"));
        div.AddEventListener("click", () => Console.WriteLine("Div clicked!"));

        Console.WriteLine(div.OuterHTML);

        // Simulate a click event
        div.NotifyEvent("click");
    }
}
