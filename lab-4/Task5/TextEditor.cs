public class TextEditor
{
    private TextDocument _textDocument = new TextDocument();
    private Stack<TextDocumentMemento> _history = new Stack<TextDocumentMemento>();

    public void Type(string words)
    {
        _history.Push(new TextDocumentMemento(_textDocument.Content));
        _textDocument.Content += words;
    }

    public void Undo()
    {
        if (_history.Count > 0)
        {
            _textDocument.Content = _history.Pop().Content;
        }
        else
        {
            Console.WriteLine("Cannot undo, history is empty.");
        }
    }

    public void PrintContent()
    {
        Console.WriteLine(_textDocument.Content);
    }
}
