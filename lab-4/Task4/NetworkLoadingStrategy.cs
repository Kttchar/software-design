public class NetworkLoadingStrategy : IImageLoadingStrategy
{
    public void LoadImage(string href)
    {
        Console.WriteLine($"Loading image from network: {href}");
    }
}