public class FileSystemLoadingStrategy : IImageLoadingStrategy
{
    public void LoadImage(string href)
    {
        Console.WriteLine($"Loading image from file system: {href}");
    }
}
