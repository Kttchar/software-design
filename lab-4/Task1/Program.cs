﻿class Program
{
    static void Main(string[] args)
    {
        var levelOneHandler = new LevelOneHandler();
        var levelTwoHandler = new LevelTwoHandler();
        var levelThreeHandler = new LevelThreeHandler();
        var levelFourHandler = new LevelFourHandler();

        levelOneHandler.SetNext(levelTwoHandler).SetNext(levelThreeHandler).SetNext(levelFourHandler);

        while (true)
        {
            Console.WriteLine("Please enter your support level (1-4):");
            int level = Convert.ToInt32(Console.ReadLine());
            levelOneHandler.Handle(level);
        }
    }
}
