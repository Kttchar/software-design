﻿using System;

public interface IDevice
{
    string Name { get; }
}

public class Laptop : IDevice
{
    public string Name { get; private set; }

    public Laptop(string brand)
    {
        Name = $"{brand} Laptop";
    }
}

public class Netbook : IDevice
{
    public string Name { get; private set; }

    public Netbook(string brand)
    {
        Name = $"{brand} Netbook";
    }
}

public class EBook : IDevice
{
    public string Name { get; private set; }

    public EBook(string brand)
    {
        Name = $"{brand} EBook";
    }
}

public class Smartphone : IDevice
{
    public string Name { get; private set; }

    public Smartphone(string brand)
    {
        Name = $"{brand} Smartphone";
    }
}

public abstract class DeviceFactory
{
    public abstract IDevice CreateLaptop();
    public abstract IDevice CreateNetbook();
    public abstract IDevice CreateEBook();
    public abstract IDevice CreateSmartphone();
}

public class IProneFactory : DeviceFactory
{
    public override IDevice CreateLaptop() => new Laptop("IProne");
    public override IDevice CreateNetbook() => new Netbook("IProne");
    public override IDevice CreateEBook() => new EBook("IProne");
    public override IDevice CreateSmartphone() => new Smartphone("IProne");
}

public class KiaomiFactory : DeviceFactory
{
    public override IDevice CreateLaptop() => new Laptop("Kiaomi");
    public override IDevice CreateNetbook() => new Netbook("Kiaomi");
    public override IDevice CreateEBook() => new EBook("Kiaomi");
    public override IDevice CreateSmartphone() => new Smartphone("Kiaomi");
}

public class BalaxyFactory : DeviceFactory
{
    public override IDevice CreateLaptop() => new Laptop("Balaxy");
    public override IDevice CreateNetbook() => new Netbook("Balaxy");
    public override IDevice CreateEBook() => new EBook("Balaxy");
    public override IDevice CreateSmartphone() => new Smartphone("Balaxy");
}

public class Program
{
    public static void Main()
    {
        DeviceFactory factory = new IProneFactory();
        Console.WriteLine(factory.CreateLaptop().Name);
        Console.WriteLine(factory.CreateNetbook().Name);
        Console.WriteLine(factory.CreateEBook().Name);
        Console.WriteLine(factory.CreateSmartphone().Name);

        factory = new KiaomiFactory();
        Console.WriteLine(factory.CreateLaptop().Name);
        Console.WriteLine(factory.CreateNetbook().Name);
        Console.WriteLine(factory.CreateEBook().Name);
        Console.WriteLine(factory.CreateSmartphone().Name);

        factory = new BalaxyFactory();
        Console.WriteLine(factory.CreateLaptop().Name);
        Console.WriteLine(factory.CreateNetbook().Name);
        Console.WriteLine(factory.CreateEBook().Name);
        Console.WriteLine(factory.CreateSmartphone().Name);
    }
}
