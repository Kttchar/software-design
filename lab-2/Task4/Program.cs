﻿using System;
using System.Collections.Generic;

public class Virus : ICloneable
{
    public string Name { get; set; }
    public string Type { get; set; }
    public double Weight { get; set; }
    public int Age { get; set; }
    public List<Virus> Children { get; set; }

    public Virus()
    {
        Children = new List<Virus>();
    }

    public object Clone()
    {
        var clone = (Virus)this.MemberwiseClone();
        clone.Children = new List<Virus>(this.Children.Count);
        foreach (Virus child in this.Children)
        {
            clone.Children.Add((Virus)child.Clone());
        }
        return clone;
    }
}

public class Program
{
    public static void Main()
    {
        //virus family
        var parent = new Virus { Name = "Parent", Type = "ParentType", Weight = 1.0, Age = 10 };
        var child1 = new Virus { Name = "Child1", Type = "ChildType", Weight = 0.5, Age = 5 };
        var grandChild1 = new Virus { Name = "GrandChild1", Type = "GrandChildType", Weight = 0.25, Age = 2 };
        var child2 = new Virus { Name = "Child2", Type = "ChildType", Weight = 0.5, Age = 5 };
        child1.Children.Add(grandChild1);
        parent.Children.Add(child1);
        parent.Children.Add(child2);

        //parent virus
        var clonedParent = (Virus)parent.Clone();

        //details of the original and cloned viruses
        Console.WriteLine($"Original parent: {parent.Name}, {parent.Type}, {parent.Weight}, {parent.Age}, number of children: {parent.Children.Count}");
        Console.WriteLine($"Cloned parent: {clonedParent.Name}, {clonedParent.Type}, {clonedParent.Weight}, {clonedParent.Age}, number of children: {clonedParent.Children.Count}");

        //details of the original and cloned children
        Console.WriteLine($"Original child1: {parent.Children[0].Name}, {parent.Children[0].Type}, {parent.Children[0].Weight}, {parent.Children[0].Age}, number of children: {parent.Children[0].Children.Count}");
        Console.WriteLine($"Cloned child1: {clonedParent.Children[0].Name}, {clonedParent.Children[0].Type}, {clonedParent.Children[0].Weight}, {clonedParent.Children[0].Age}, number of children: {clonedParent.Children[0].Children.Count}");
    }
}
