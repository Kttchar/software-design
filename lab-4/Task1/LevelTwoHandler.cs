class LevelTwoHandler : AbstractHandler
{
    public override void Handle(int request)
    {
        if (request == 2)
        {
            Console.WriteLine("Level 2 handler is handling the request");
        }
        else
        {
            base.Handle(request);
        }
    }
}