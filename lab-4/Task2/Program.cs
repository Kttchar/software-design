﻿namespace DesignPatterns.Mediator
{
   

    class Program
    {
        static void Main(string[] args)
        {
            var commandCentre = new CommandCentre();
            var runway1 = new Runway();
            var runway2 = new Runway();
            var aircraft1 = new Aircraft("Aircraft1", commandCentre);
            var aircraft2 = new Aircraft("Aircraft2", commandCentre);

            commandCentre.RegisterRunway(runway1);
            commandCentre.RegisterRunway(runway2);
            commandCentre.RegisterAircraft(aircraft1);
            commandCentre.RegisterAircraft(aircraft2);

            aircraft1.Land();
            aircraft2.Land();
            aircraft1.TakeOff();
            aircraft2.Land();
        }
    }
}
