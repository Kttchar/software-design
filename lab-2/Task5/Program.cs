﻿public class Character
{
    public string Name { get; set; }
    public int Height { get; set; }
    public int Weight { get; set; }
    public string HairColor { get; set; }
    public string EyeColor { get; set; }
    public List<string> Clothes { get; set; }
    public List<string> Inventory { get; set; }

    public Character()
    {
        Clothes = new List<string>();
        Inventory = new List<string>();
    }
}

public abstract class CharacterBuilder
{
    protected Character character;

    public Character GetCharacter()
    {
        return character;
    }

    public CharacterBuilder CreateNewCharacter()
    {
        character = new Character();
        return this;
    }

    public abstract CharacterBuilder SetName(string name);
    public abstract CharacterBuilder SetHeight(int height);
    public abstract CharacterBuilder SetWeight(int weight);
    public abstract CharacterBuilder SetHairColor(string color);
    public abstract CharacterBuilder SetEyeColor(string color);
    public abstract CharacterBuilder SetClothes(List<string> clothes);
    public abstract CharacterBuilder SetInventory(List<string> inventory);
}

public class HeroBuilder : CharacterBuilder
{
    public override CharacterBuilder SetName(string name)
    {
        character.Name = name;
        return this;
    }

    public override CharacterBuilder SetHeight(int height)
    {
        character.Height = height;
        return this;
    }

    public override CharacterBuilder SetWeight(int weight)
    {
        character.Weight = weight;
        return this;
    }

    public override CharacterBuilder SetHairColor(string color)
    {
        character.HairColor = color;
        return this;
    }

    public override CharacterBuilder SetEyeColor(string color)
    {
        character.EyeColor = color;
        return this;
    }

    public override CharacterBuilder SetClothes(List<string> clothes)
    {
        character.Clothes = clothes;
        return this;
    }

    public override CharacterBuilder SetInventory(List<string> inventory)
    {
        character.Inventory = inventory;
        return this;
    }
}

public class EnemyBuilder : CharacterBuilder
{
    public override CharacterBuilder SetName(string name)
    {
        character.Name = name;
        return this;
    }

    public override CharacterBuilder SetHeight(int height)
    {
        character.Height = height;
        return this;
    }

    public override CharacterBuilder SetWeight(int weight)
    {
        character.Weight = weight;
        return this;
    }

    public override CharacterBuilder SetHairColor(string color)
    {
        character.HairColor = color;
        return this;
    }

    public override CharacterBuilder SetEyeColor(string color)
    {
        character.EyeColor = color;
        return this;
    }

    public override CharacterBuilder SetClothes(List<string> clothes)
    {
        character.Clothes = clothes;
        return this;
    }

    public override CharacterBuilder SetInventory(List<string> inventory)
    {
        character.Inventory = inventory;
        return this;
    }
}

public class Director
{
    public Character Construct(CharacterBuilder builder, string name, int height, int weight, string hairColor, string eyeColor, List<string> clothes, List<string> inventory)
    {
        return builder.CreateNewCharacter()
                      .SetName(name)
                      .SetHeight(height)
                      .SetWeight(weight)
                      .SetHairColor(hairColor)
                      .SetEyeColor(eyeColor)
                      .SetClothes(clothes)
                      .SetInventory(inventory)
                      .GetCharacter();
    }
}

public class Program
{
    public static void Main()
    {
        Director director = new Director();

        HeroBuilder heroBuilder = new HeroBuilder();
        List<string> heroClothes = new List<string> { "Armor", "Helmet" };
        List<string> heroInventory = new List<string> { "Sword", "Shield" };
        Character hero = director.Construct(heroBuilder, "Hero", 180, 80, "Black", "Blue", heroClothes, heroInventory);
        Console.WriteLine($"Created {hero.Name} with height: {hero.Height}, weight: {hero.Weight}, hair color: {hero.HairColor}, eye color: {hero.EyeColor}, clothes: {string.Join(", ", hero.Clothes)}, and inventory: {string.Join(", ", hero.Inventory)}");

        EnemyBuilder enemyBuilder = new EnemyBuilder();
        List<string> enemyClothes = new List<string> { "Robe", "Hood" };
        List<string> enemyInventory = new List<string> { "Staff", "Potion" };
        Character enemy = director.Construct(enemyBuilder, "Enemy", 200, 100, "Red", "Green", enemyClothes, enemyInventory);
        Console.WriteLine($"Created {enemy.Name} with height: {enemy.Height}, weight: {enemy.Weight}, hair color: {enemy.HairColor}, eye color: {enemy.EyeColor}, clothes: {string.Join(", ", enemy.Clothes)}, and inventory: {string.Join(", ", enemy.Inventory)}");
    }
}
