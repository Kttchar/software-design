﻿using System;

public sealed class Authenticator
{
    private static readonly Lazy<Authenticator> lazy = new Lazy<Authenticator>(() => new Authenticator());

    public static Authenticator Instance { get { return lazy.Value; } }

    private Authenticator()
    {
    }
}

public class Program
{
    public static void Main()
    {
        Authenticator auth1 = Authenticator.Instance;
        Authenticator auth2 = Authenticator.Instance;

        if (auth1 == auth2)
        {
            Console.WriteLine("Both instances are the same.");
        }
        else
        {
            Console.WriteLine("Instances are different.");
        }
    }
}
