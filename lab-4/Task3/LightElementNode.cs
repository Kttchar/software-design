using System.Text;

public class LightElementNode : LightNode
{
    private string tagName;
    private bool isBlock;
    private bool isSelfClosing;
    private List<string> classes;
    private List<LightNode> children;
    private Dictionary<string, List<EventListener>> eventListeners;

    public LightElementNode(string tagName, bool isBlock, bool isSelfClosing)
    {
        this.tagName = tagName;
        this.isBlock = isBlock;
        this.isSelfClosing = isSelfClosing;
        this.classes = new List<string>();
        this.children = new List<LightNode>();
        this.eventListeners = new Dictionary<string, List<EventListener>>();
    }

    public void AddClass(string className)
    {
        classes.Add(className);
    }

    public void AddChild(LightNode child)
    {
        children.Add(child);
    }

    public void AddEventListener(string eventName, Action handler)
    {
        if (!eventListeners.ContainsKey(eventName))
        {
            eventListeners[eventName] = new List<EventListener>();
        }
        eventListeners[eventName].Add(new EventListener(handler));
    }

    public void RemoveEventListener(string eventName, EventListener listener)
    {
        if (eventListeners.ContainsKey(eventName))
        {
            eventListeners[eventName].Remove(listener);
        }
    }

    public void NotifyEvent(string eventName)
    {
        if (eventListeners.ContainsKey(eventName))
        {
            foreach (var listener in eventListeners[eventName])
            {
                listener.Update();
            }
        }
    }

    public override string OuterHTML
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"<{tagName}");
            if (classes.Count > 0)
            {
                sb.Append($" class=\"{string.Join(" ", classes)}\"");
            }
            sb.Append(">");
            if (!isSelfClosing)
            {
                sb.Append(InnerHTML);
                sb.Append($"</{tagName}>");
            }
            if (isBlock)
            {
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }

    public override string InnerHTML
    {
        get
        {
            StringBuilder sb = new StringBuilder();
            foreach (LightNode child in children)
            {
                sb.Append(child.OuterHTML);
            }
            return sb.ToString();
        }
    }
}
