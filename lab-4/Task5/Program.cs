﻿class Program
{
    static void Main(string[] args)
    {
        TextEditor editor = new TextEditor();

        editor.Type("First sentence. ");
        editor.PrintContent();

        editor.Type("Second sentence. ");
        editor.PrintContent();

        editor.Type("Third sentence. ");
        editor.PrintContent();

        editor.Type("Fourth sentence. ");
        editor.PrintContent();

        editor.Undo();
        editor.PrintContent();

        editor.Undo();
        editor.PrintContent();

        editor.Undo();
        editor.PrintContent();

    }
}
