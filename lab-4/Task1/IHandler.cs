interface IHandler
{
    IHandler SetNext(IHandler handler);
    void Handle(int request);
}
