
class CommandCentre : ICommandCentre
{
    private List<Runway> _runways = new List<Runway>();
    private List<Aircraft> _aircrafts = new List<Aircraft>();

    public void RegisterRunway(Runway runway)
    {
        _runways.Add(runway);
    }

    public void RegisterAircraft(Aircraft aircraft)
    {
        _aircrafts.Add(aircraft);
    }

    public void RequestLanding(Aircraft aircraft)
    {
        foreach (var runway in _runways)
        {
            if (runway.IsBusyWithAircraft == null)
            {
                Console.WriteLine($"Aircraft {aircraft.Name} is landing.");
                Console.WriteLine($"Checking runway.");
                Console.WriteLine($"Aircraft {aircraft.Name} has landed.");
                runway.IsBusyWithAircraft = aircraft;
                runway.HighLightRed();
                return;
            }
        }
        Console.WriteLine($"Could not land, all runways are busy.");
    }

    public void NotifyTakeOff(Aircraft aircraft)
    {
        foreach (var runway in _runways)
        {
            if (runway.IsBusyWithAircraft == aircraft)
            {
                Console.WriteLine($"Aircraft {aircraft.Name} is taking off.");
                runway.IsBusyWithAircraft = null;
                runway.HighLightGreen();
                Console.WriteLine($"Aircraft {aircraft.Name} has took off.");
                return;
            }
        }
    }
}
