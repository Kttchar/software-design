class LevelFourHandler : AbstractHandler
{
    public override void Handle(int request)
    {
        if (request == 4)
        {
            Console.WriteLine("Level 4 handler is handling the request");
        }
        else
        {
            Console.WriteLine("No handler found for this level. Repeating the menu.");
        }
    }
}
