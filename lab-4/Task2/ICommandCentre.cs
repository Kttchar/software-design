using DesignPatterns.Mediator;

interface ICommandCentre
{
    void RegisterRunway(Runway runway);
    void RegisterAircraft(Aircraft aircraft);
    void RequestLanding(Aircraft aircraft);
    void NotifyTakeOff(Aircraft aircraft);
}