
public class EventListener : IObserver
{
    private Action handler;

    public EventListener(Action handler)
    {
        this.handler = handler;
    }

    public void Update()
    {
        handler.Invoke();
    }
}
