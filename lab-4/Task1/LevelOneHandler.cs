
class LevelOneHandler : AbstractHandler
{
    public override void Handle(int request)
    {
        if (request == 1)
        {
            Console.WriteLine("Level 1 handler is handling the request");
        }
        else
        {
            base.Handle(request);
        }
    }
}