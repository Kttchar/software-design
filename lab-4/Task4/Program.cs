﻿class Program
{
    static void Main(string[] args)
    {
        ImageElement image = new ImageElement(new FileSystemLoadingStrategy());

        image.LoadImage("C:/Users/derde/Downloads/pngwing.com.png");

        image.SetLoadingStrategy(new NetworkLoadingStrategy());

        image.LoadImage("https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/C_Sharp_Logo_2023.svg/1280px-C_Sharp_Logo_2023.svg.png");
    }
}
